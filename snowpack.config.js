// Snowpack Configuration File
// See all supported options: https://www.snowpack.dev/reference/configuration

/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
	mount: {
		src: '/'
	},
	plugins: [
		['@snowpack/plugin-sass',
			{
				style: 'compressed',
				loadPath: 'assets/scss'
			}],
	],
	packageOptions: {
		/* ... */
	},
	devOptions: {
		/* ... */
	},
	buildOptions: {
		out: 'public'
	},
};
